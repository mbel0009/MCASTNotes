'''Radio Worksheet'''
import random

# Question 1
def start_radio_1():
    '''Question 1 - Simple function that displays radio is starting'''
    print("Radio is starting")

start_radio_1()

# Question 2
def random_number():
    '''Question 2 - Simple function that generates a random number'''
    print(random.randint(5, 10))

random_number()

# Question 2 v.2
def generateNumber(b,e):
    rand = random.radint()
    return rand

randnum = generateNumber(5, 10)
print(randnum)

# Question 3
NAME = "88.7 - Vibe FM"

def start_radio_2(name):
    '''Question 3 - Simple function that has a parameter and uses that in the print'''
    print("Radio", name, "is starting")

start_radio_2(NAME)

# Question 4
USERNAME = input("Enter Username: ")

CHARS = 'abcdefghijklmnopqrstuvxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-=+/*'

def generate_password():
    '''Question 4 - Simple function that generates a random password'''
    password = ''
    for c in range(10):
        password += random.choice(CHARS)
    return print("Your password is", password)

generate_password()