from gpiozero import LED
from time import sleep
import pygame # library for writing video games. It includes computer graphics and sound libraries

RED_LED1 = LED(21)
BLUE_LED1 = LED(20)

file = 'Original GhostBusters Theme Song.mp3' # Importing the file, it has to be in the same location as this file

pygame.init()
pygame.mixer.init()
pygame.mixer.music.load(file)
pygame.mixer.music.play()
