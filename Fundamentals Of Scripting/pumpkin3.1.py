from gpiozero import LED
from time import sleep

RED_LED1 = LED(21)
BLUE_LED1 = LED(20)

number = int(input("Enter number: "))

BLUE_LED1.on()
RED_LED1.on()
sleep(number) #sleep for x seconds
BLUE_LED1.off()
RED_LED1.off()