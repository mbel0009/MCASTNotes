from gpiozero import LED
from time import sleep

name = input("whats your name?")
print("welcome", name)

R_LED = LED(20)
O_LED = LED(21)

print("Red on")
R_LED.on()
sleep(2)
R_LED.off()

print("Orange on")
O_LED.on()
sleep(2)
O_LED.off()
