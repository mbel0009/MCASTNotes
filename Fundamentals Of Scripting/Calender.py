#created 3 lists
DaysOfWeek = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
Months = ["January","Febuary","March","April","May","June","July","August","September","October","November","December"]
NumMonths = [31,28,31,30,31,30,31,31,30,31,30,31] #must not be in "" as they are integers

print("== FIRST PART ==")
print("Days of the Week:")
#for loop to show all the days of the week
for day in DaysOfWeek: #for (any variable) day repeat the loop for how many variables are in the list DaysOfWeek(7)
    print(DaysOfWeek.index(day)+1,"\t", day) #name.index shows the position on where day of the week is.
    # when in the loop day is 0 - it goes in the list and starts searching from zero
    # DaysOfWeek = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    # DaysOfWeek = [   0    ,     1    ,      2     ,      3    ,     4   ,      5    ,    6    ]
    # ex. DaysOfWeek[0] is equal to Monday
    # ex. DaysOfWeek[4] is equal to Friday
    # ex. DaysOfWeek[6] is equal to Sunday

print("== SECOND PART ==")
print("Calender:")
year = input("Input a year:")
z=0 #initializing a variable for later on in the function - days of week -> zero being Monday

for x in range(12):# range is up to 12 months -- from 0 to 11 [remember that lists always start from 0]
#this for loop is used to do the functions below for all the months, first 'January' then 'Febuary', 'March'.. ect.

    print("--------------------")# just to separate from month to another
    print(Months[x],"has", NumMonths[x],"days")# same concept as above is used - the first time it goes in the for loop x is equal to 0.
                                               # therefore Month[0] is 'January', and NumMonths[0] is '31'

    for y in range(1,NumMonths[x]+1):# #for loop from 1 till the number of days of the month.
    # this for loop is used to repeat the months and print out different days of that month

        mon = Months[x] # the list is set as a variable as it would recall an error
        if z == 7: #if number of week is 7[non existing, as the list is from 0 to 6] the go in loop and start again from zero, if nor skip the if function and continue normally
            z=0
        print(y,mon,"-",DaysOfWeek[z])
        z += 1 # add 1 to z so if it is Mondays, now from the list it will become DaysOfWeek[0+1] which is equal to 'Tuesday'
