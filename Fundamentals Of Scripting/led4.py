from gpiozero import LED
from time import sleep

RED_LED1 = LED(21)
BLUE_LED1 = LED(20)

MONTH = int(input("Enter month number: "))

if MONTH == 10 or 11:
    print("Halloween!")
    while True:
        RED_LED1.on()
        BLUE_LED1.on()
        print("Red LED is on")
        print("Blue LED is on")
        sleep(0.5)
        RED_LED1.off()
        BLUE_LED1.off()
        sleep(0.5)
        print("Red LED is off")
        print("Blue LED is off")
else:
    print("No Halloween!")