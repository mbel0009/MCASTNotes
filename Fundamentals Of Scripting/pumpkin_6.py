from gpiozero import LED
from time import sleep

yellow1 = LED(4) #setup gpio 4
yellow2 = LED(26) #setup gpio 26


print("--MENU--")

print("Press [1] to turn on pumpkin")
print("Press [2] to blink pumpkin")
print("Press [3] to turn on after 5 seconds")

user_input = input("Enter your choice:")

if(user_input == "1"):
    print("Turn On")
    yellow1.on()
    yellow2.on()
elif(user_input == "2"):
    print("blink")
    yellow1.on()
    yellow2.on()
    sleep(2)
    yellow1.off()
    yellow2.off()
    sleep(2)
    yellow1.on()
    yellow2.on()
    sleep(2)
    yellow1.off()
    yellow2.off()
elif(user_input == "3"):
    print("turn on after 5 seconds")
    sleep(5)
    yellow1.on()
    yellow2.on()
else:
    print("incorrect input")




