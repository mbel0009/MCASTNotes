import random
x=0
def easy():
    genNum = random.randint(0,10)
   # print(genNum)
    return genNum

def med():
    genNum = random.randint(0, 50)
    #print(genNum)
    return genNum

def hard():
    genNum = random.randint(0, 100)
    #print(genNum)
    return genNum

def game(x):
    print("- HiLo: GUESS THE NUMBER -")
    print("Select Level:")
    print("1. Easy")
    print("2. Medium")
    print("3. Hard")
    level = int(input("Level selected:"))
    while True:
        if level == 1:
            rng = 11
            genNum = easy()
            break
        elif level == 2:
            rng = 51
            genNum = med()
            break
        elif level == 3:
            rng = 101
            genNum = hard()
            break
        else:
            print("Input not valid")#looop repeating
    while True:
        num = int(input("Enter a number:"))
        x += 1
        if num < rng:
            if genNum > num:
                print("Nope, try a higher number")
            if genNum < num:
                print("Nope, try a lower number")
            if genNum == num:
                print("You Won!")
                print("You have guessed the number after", x, "times")
                break
            if x == 10:
                print("GAME OVER")
                break
        else:
            print("Enter number within the range")
game(x)