from gpiozero import LED
from time import sleep

ans = input("Do you want to switch on the LED?")

R_LED = LED(20)

if ans == 'yes':
    print("SWITCH IS ON")
    R_LED.on()
    O_LED.on()
    sleep(2)
    R_LED.off()
    O_LED.off()

elif ans == 'no':
    print("STILL OFF")

else:
    print("OFF: INVALID INPUT")
