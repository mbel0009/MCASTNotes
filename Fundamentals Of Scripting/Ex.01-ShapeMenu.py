#Simple Program which asks the user to select a shape from the menu
#then outputs the area of the selected shape

def start_shapes_menu():
    '''Simple Shape Menu - First Menu the user will see'''

    print(20 * "-", "Shape Menu", 20 * "-")
    print("1. Sqaure")
    print("2. Rectangle")
    print("3. Triangle")
    print(52 * "-")

    choose_shape()

def choose_shape():
    '''Choosing a shape to continue the calculation'''

    #Asking the user to enter one of the shapes from the menu and converting the choiceShape to int
    choice_shape = int(input("Choose one of the shapes, by typing 1-3: "))

    #Simple if block statement which outputs which shape the user has selected
    if choice_shape == 1:
        print("\nUser chose a Square")
        choice_process = process_shape()
        calculate_on_square(choice_process)
    elif choice_shape == 2:
        print("\nUser chose a Rectangle")
        choice_process = process_shape()
        calculate_on_rectangle(choice_process)
    elif choice_shape == 3:
        print("\nUser chose a Triangle")
        choice_process = process_shape()
        calculate_on_triangle(choice_process)
    else:
        start_shapes_menu()

def process_shape():
    ''' k '''

    #Process/Formula Menu - Second Menu the user will see
    print("\n", 19 * "-", "Process Menu", 19 * "-")
    print("1. Area")
    print("2. Perimeter")
    print(52 * "-")

    #Asking the user to enter one of the formulas he wants to perform and converting the choiceProcess to int
    choice_process = int(input("Choose one of the formulas, by typing 1 or 2: "))

    if (choice_process == 1 or choice_process == 2):
        return choice_process
    else:
        return process_shape()

def calculate_on_square(choice_process):
    ''' k '''

    #simple if statements which checks the choiceShape and choiceProcess
    if choice_process == 1:
        print("\nUser chose to find the area of a square;\n")
        length = float(input("Length of the side is: ")) #Asking the user to enter the length, and converting it to float
        print("The Area is ", (length*length), "cm^2") #displaying the area
    elif choice_process == 2:
        print("\nUser chose to find the perimeter of a square;\n")
        length = float(input("Length of the side is: ")) #Asking the user to enter the length, and converting it to float
        print("The Perimeter is", (length*4), "cm") #displaying the perimeter

def calculate_on_rectangle(choice_process):
    ''' k '''

    if choice_process == 1:
        print("\nUser chose to find the area of a rectangle;\n")
        length = float(input("Length of side is: "))
        width = float(input("Width of side is: "))
        print("The Area is", (length*width), "cm^2")
    elif choice_process == 2:
        print("\nUser chose to find the perimeter of a rectangle;\n")
        length = float(input("Length of side is: "))
        width = float(input("Width of side is: "))
        print("The Perimeter is", (2*(length+width)), "cm")

def calculate_on_triangle(choice_process):
    ''' k '''

    if choice_process == 1:
        print("\nUser chose to find the the area of a triangle;\n")
        base = float(input("Length of the base is: "))
        height = float(input("Length of the height is: "))
        print("The Area is", ((base*height)/2), "cm^2")
    elif choice_process == 2:
        print("\nUser chose to find the perimeter of a triangle;\n")
        length1 = float(input("Length of side 1 is: "))
        length2 = float(input("Length of side 2 is: "))
        length3 = float(input("Length of side 3 is: "))
        print("The Perimeter is", (length1+length2+length3), "cm")

start_shapes_menu()
