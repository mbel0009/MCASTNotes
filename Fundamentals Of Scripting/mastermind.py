import random
'''Mastermind'''

# Instructions
'''
0-9
user input 4 numbers
blue - found and correct position
red - not found
'''

nums = []

def guess():
    print("- MASTERMIND -")
    print("Enter 4 unique numbers separated with spaces ")
    x = [int(i) for i in input().split()]
    print(x)
    gen_four_numbers(x)


def generate_random_number():
    '''Generates one random numbers'''
    random_number = random.randint(0, 9)
    return random_number

def equal_numbers(n_1, n_2):
    '''Compare two integers'''
    if n_1 == n_2:
        return True
    else:
        return False

def equal_list(n_1, n_2):
    '''Compare two integers from both lists'''
    if n_1 == n_2:
        return True
    else:
        return False

def gen_four_numbers(x):
    '''Generates four random numbers'''
    nums_found = 0
    if nums == []:
        for count in range (0, 4):
            num = generate_random_number()
            nums.append(num)
            if count > 0:
                for count2 in range (count):
                    while True:
                        if equal_numbers(nums[count], nums[count2]):
                            nums[count] = generate_random_number()
                        else:
                            break
    #print(nums)

    for c in range(4):
        if equal_list(nums[c], x[c]):
            print(nums[c], "is found")
            nums_found += 1
        #else:
        #    print("not found")
    print("==========")
    print(nums_found, "number(s) found")
    #print(nums)
    if nums == x:
        print("All found")
    else:
        guess()

guess()

while True:
    TRY_AGAIN = input("\nTry again? ('Y'): ")
    if TRY_AGAIN.upper() == "Y":
        nums = []
        guess()
    else:
        input("bye.[press enter to exit]")
        exit()
