#Example 1.1

#write to a file (overwrite)
fh = open("test.txt","w")
fh.write("HELLO THERE")
fh.close()

#Example 1.2

#add to a file (append)
fh = open("test.txt","a")
fh.write("\nTHANKS FOR COMING TO OUR SHOP")
fh.close()

#Example 1.3
#read from file

fh = open("test.txt","r")
content = fh.read()
fh.close()
print(content)

#Example 1.4
products = ["Mouse","Keyboard","Laptop"]
for product in products:
    fh = open("test.txt","a")
    fh.write("\n"+product)
    fh.close()

#Example 2.1

#general exception handling
while(True):
    try:
        x = float(input("Enter product price:")) #user might enter string
        print(x/0) #zero division error

    except Exception as ex:
        print("error",ex)

#Example 2.2

while(True):
    try:
        x = float(input("Enter product price:")) #user might enter string
        print(x/0) #zero division error

    except ValueError:
        print("Enter correct number")

    except ZeroDivisionError:
        print("You are dividing by zero....")

    except Exception as ex:
        print("error",ex)

#control C - is a command interrupt-- to stop program