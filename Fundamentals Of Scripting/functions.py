from gpiozero import LED
from time import sleep

RED_LED1 = LED(21)
BLUE_LED1 = LED(20)

def login():
    while True:
        username = input("Enter Username: ")
        password = input("Enter Password: ")
        password = int(password)
        if username == 'John' and password == 123:
            print("Valid")
            break
        else:
            print("Invalid")


def duration():
    while True:
        sec = int(input("Enter number of seconds: "))
        if sec >= 0:
            break
        else:
            print("Invalid input")
    return sec

def blinks(num):
    while True:
        question = int(input("Enter number of blinks: "))
        if question >= 0:
            for x in range(question):
                BLUE_LED1.on()
                sleep(num)
                BLUE_LED1.off()
                sleep(num)
            break
        else:
            print("Invalid input")


login()
sec = duration()
blinks(sec)