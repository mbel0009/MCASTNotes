##install mpc library -> sudo apt-get install mpd mpc
##mpc player can be controlled in terminal using the following commands: https://linux.die.net/man/1/mpc
##change audio output to 3.5mm jack ->  https://www.raspberrypi.org/documentation/configuration/audio-config.md

from gpiozero import LED
from time import sleep
import subprocess #with python we can open the terminal and execute commands
import random

green = LED(17) #setup gpio 17
blue = LED(27) #setup gpio 27
number_of_radios = 11


#add all radio channels to mpc.
subprocess.call("mpc clear ",shell=True) #first clear mpc of any existing radio stations
#subprocess.call("mpc add http://s38.myradiostream.com:6982",shell=True) #add radio malta 1 (index 0)
#subprocess.call("mpc add http://s11.myradiostream.com:4954",shell=True) #add radio malta 2 (index 1)
#subprocess.call("mpc add http://s46.myradiostream.com:6076",shell=True) #add magic malta (index 2)
#subprocess.call("mpc add http://178.32.161.194:8022",shell=True) #add bay radio (index 3)
#subprocess.call("mpc add http://s9.voscast.com:7824/",shell=True) #add vibe fm (index 4)
#subprocess.call("mpc add http://media-ice.musicradio.com/ClassicFMMP3",shell=True) #add classic fm (index 5)
#subprocess.call("mpc add http://dreamsiteradiocp2.com:8096",shell=True) #add radju marija (index 6)
subprocess.call("mpc add http://live.radiom.fr/live600.opus", shell=True)
subprocess.call("mpc add http://media-ice.musicradio.com/ClassicFMMP3", shell=True)
subprocess.call("mpc add http://iradio.fi:8000/klasupro.flac", shell=True)
subprocess.call("mpc add http://listen.42fm.ru:8000/stealkill-3.0.ogg", shell=True)

##load functions in memory

def blink_green_led(number_of_times):
    for i in range(0,number_of_times):
        green.on()
        sleep(1)
        green.off()
        sleep(1)

def blink_blue_led():
    blue.on()
    sleep(1)
    blue.off()


def load_next_station():
    print("loading next station...")
    subprocess.call("mpc next ",shell=True) #load next station
    blink_blue_led()

def load_prev_station():
    print("loading previous station...")
    subprocess.call("mpc prev ",shell=True) #load previous station
    blink_blue_led()

def increase_volume():    
    subprocess.call("mpc volume +5" ,shell=True) #increase volume by 5
    print("volume increased by 5...") 

def decrease_volume():
    subprocess.call("mpc volume -5" ,shell=True) #decrease volume by 5
    print("volume decreased by 5...")

def decrease_volume_by_user_input(vol):
    subprocess.call("mpc volume -"+vol ,shell=True) #decrease volume specified by the user
    print("volume decreased...")

def increase_volume_by_user_input(vol):
    subprocess.call("mpc volume +"+vol ,shell=True) #increase volume specified by the user
    print("volume increased...")


##START

blink_green_led(3) #blink for 3 times
subprocess.call("mpc play ",shell=True) #start player

name = input("Enter your name:")

while True: #start menu
    print("----Hello",name,".Welcome to the best Maltese Radio System----")
    print("Press [a] to load previous station")
    print("Press [d] to load next station")
    print("Press [w] to increase volume by 5")
    print("Press [s] to decrease volume by 5")
    print("Press [r] to increase/decrease volume by user input")
    print("Press [t] to play all radio stations and change every 10 seconds")
    print("Press [p] to play a random station")
    print("Press [z] to exit")
    
    user_input = input("Enter menu choice:").lower()
    
    if user_input == "a" :
        load_prev_station()
    elif user_input == "d":
        load_next_station()
    elif user_input == "w":
        increase_volume()
    elif user_input == "s":
        decrease_volume()
    elif user_input == "r":
        user_input1 = input("Do you want to increase volume? Y/N:").lower()
        user_input2 = input("By how much?")
        if user_input1 == "y":
            increase_volume_by_user_input(user_input2)
        else:
            decrease_volume_by_user_input(user_input2)
            
    elif user_input == "t":
        subprocess.call("mpc play 1",shell=True) #play first radio station (index + 1)
        sleep(10)
        for x in range(2,number_of_radios+1): #play radio station one by one till the end
            subprocess.call("mpc next",shell=True)
            print("changed station")
            sleep(10)
            
    elif user_input == "p":
        r = random.randint(1,number_of_radios) #generates a random number between 1 and 7 - (1 and 7 inclusive)
        subprocess.call("mpc play "+str(r),shell=True) #play radio station by index

    elif user_input == "z":
        subprocess.call("mpc stop",shell=True) #stop radio
        break #stop while loop
