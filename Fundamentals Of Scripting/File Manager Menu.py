import random
import sys
from datetime import datetime
# Textual month, day and year
now = datetime.now()

items = []
prices = []

ranNum = random.randint(1,6)

def openLane():
    print("Lane Opened")


def closeLane():
    print("Lane closed")


def addItem(item_name, item_price):
    items.append(item_name)
    prices.append(item_price)


def displayAllItems():
    index = 0
    for item in items:
        print("Item: ", item, ",\t Price:", prices[index])
        index = index + 1


def countItems():
    number_of_items = len(items)
    return number_of_items


def checkout():
    print("------RECEIPT------")
    print("-SUPER MARIO SUPERMARKET-")
    total = 0
    index = 0
    for item in items:
        print("Item: ", item, ", Price: $", prices[index])

        total = total + prices[index]
        index = index + 1

    print("Total Price:", total)


def checkoutWithFileSave(ranNum):
    dataToSave = "------RECEIPT------\n"
    dataToSave += "-SUPER MARIO SUPERMARKET-\n"
    total = 0
    index = 0
    num = int(input("Insert a number between 1-5:"))

    print(ranNum)

    if ranNum == num:
        print("   You have guessed the number!  ")
        print("*** You won a free water pack ***")
        water = "Item: Water pack, Price:Free"
    else:
        print("Wrong Number. no gifts :(")
        water = ""
    for item in items:
        dataToSave += "Item: " + item + ",\t Price: $" + str(prices[index]) + "\n"

        total = total + prices[index]
        index = index + 1

    dataToSave += water
    dataToSave += "\n" + "Total Price:" + str(total) + "\n"

    # save to file
    fh = open("output.txt", "w")
    fh.write(dataToSave)
    fh.close()
    return num

def FileSaveDate(ranNum):
    dataToSave = "\n------RECEIPT-DATE------\n"
    dataToSave += "-SUPER MARIO SUPERMARKET-\n"
    total = 0
    index = 0

    num = int(input("Insert a number between 1-5:"))

    if ranNum == num:
        print("   You have guessed the number!  ")
        print("*** You won a free water pack ***")
        water = "Item: Water pack, Price:Free"
    else:
        print("Wrong Number. no gifts :(")
        water = ""

    for item in items:

        dt = now.strftime("%d/%m/%Y %H:%M:%S ")
        dataToSave += dt + "Item: " + item + ",\t Price: $" + str(prices[index]) + "\n"

        total = total + prices[index]
        index = index + 1

    dataToSave += water
    dataToSave += "Total Price: $" + str(total)

    # save to file
    fh = open("output.txt", "a")
    fh.write(dataToSave)
    fh.close()


while (True):
    print("-----Lane Manager-----")
    print("[1] Open/Close Lane")
    print("[2] Display all items")
    print("[3] Add Item with price")
    print("[4] Count Items")
    print("[5] Checkout")
    print("[6] Checkout With File Save")
    print("[7] Checkout With File Save with Date")
    print("[Q] Exit Menu")

    x = input("Enter choice:")
    if (x == "1"):
        ans = input("Do you want to open lane?y/n")
        if (ans == "y"):
            openLane()
        else:
            closeLane()

    elif (x == "2"):
        displayAllItems()
    elif (x == "3"):
        item_description = input("Enter item description:")

        # Exception handling
        while (True):
            try:
                item_price = float(input("Enter item price:"))
                break
            except:
                print("An exception occurred - Enter a valid number")

        addItem(item_description, item_price)
    elif (x == "4"):
        numb_items = countItems()
        print("Number of items:", numb_items)
    elif (x == "5"):
        checkout()
    elif (x == "6"):
        checkoutWithFileSave(ranNum)
    elif (x == "7"):
        FileSaveDate(ranNum)
    elif (x == "Q"):
        sys.exit()