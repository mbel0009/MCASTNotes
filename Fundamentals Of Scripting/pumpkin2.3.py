from gpiozero import LED
from time import sleep

RED_LED1 = LED(21)
BLUE_LED1 = LED(20)

number_1 = int(input("Enter first number: "))
number_2 = int(input("Enter second number: "))

if (number_1 == number_2):
    print("Numbers are equal")
    RED_LED1.on()
    BLUE_LED1.on()
    sleep(5)
    RED_LED1.off()
    BLUE_LED1.off()
else:
    print("Numbers are not equal")
    RED_LED1.on()
    BLUE_LED1.on()
    sleep(3)
    RED_LED1.off()
    BLUE_LED1.off()