from gpiozero import LED
from time import sleep

RED_LED1 = LED(21)
BLUE_LED1 = LED(20)

number_1 = int(input("Enter first number: "))
number_2 = int(input("Enter second number: "))
number_3 = int(input("Enter third number: "))

largest = number_1

if number_2 > largest:
    largest = number_2
if number_3 > largest:
    largest = number_3

RED_LED1.on()
BLUE_LED1.on()
sleep(largest)
RED_LED1.off()
BLUE_LED1.off()