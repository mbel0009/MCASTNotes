import random

'''Random Game'''

# Instructions
'''
Generate 5 random numbers between 1 and 42
Show them
Numbers must be random and different from each other
'''

def generate_random_number():
    '''Generates one random numbers'''
    random_number = random.randint(1, 42)
    return random_number

def equal_numbers(n_1, n_2):
    '''Compare two integers'''
    if n_1 == n_2:
        return True
    else:
        return False

def five_numbers():
    '''Generates five random numbers'''

    nums = []

    for count in range (0, 5):
        num = generate_random_number()
        nums.append(num)
        if count > 0:
            for count2 in range (count):
                while True:
                    if equal_numbers(nums[count], nums[count2]):
                        nums[count] = generate_random_number()
                    else:
                        break
    print(nums)

five_numbers()

TRY_AGAIN = input("\nTry again? ('Y'): ")
if TRY_AGAIN.upper() == "Y":
    five_numbers()
else:
    exit()
