from gpiozero import LED
from time import sleep

RED_LED1 = LED(21)
BLUE_LED1 = LED(20)

while True:
    RED_LED1.on()
    BLUE_LED1.on()
    sleep(0.5)
    RED_LED1.off()
    BLUE_LED1.off()
    sleep(0.5)
else:
    print("No inifinte loop")