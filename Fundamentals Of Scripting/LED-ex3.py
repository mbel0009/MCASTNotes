from gpiozero import LED
from time import sleep

month = input("input number month:")
month = int(month)

R_LED = LED(20)
O_LED = LED(21)

if month == 10:
    print("HALLOWEEEN")
    for x in range(5):
        R_LED.on()
        O_LED.on()
        sleep(0.5)
        R_LED.off()
        O_LED.off()
