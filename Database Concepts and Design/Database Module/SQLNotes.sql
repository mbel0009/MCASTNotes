CREATE DATABASE VideoShop; -- This statement creates a new database called VideoShop.

USE VideoShop;
GO 

-- The above statement switched the script to use the new database VideoShop.

CREATE SCHEMA store; 
/*	The above statement creates a new schema in the Database VideoShop.
	The schema is used to group tables accordung to subject. */

CREATE TABLE store.Customer ( -- table the naming format schemaname.tablename
	Customer_Id INTEGER PRIMARY KEY, -- this attribute customer_Id has the datatype integer and is also set as the primary key
	First_Name VARCHAR(25), -- the first name has a datatype has a varchar which allows character of a maximum of 25
	Last_Name VARCHAR(30) NOT NULL, -- the last name has a constraint of 'NOT NULL' meaning each record must have a value for this attribute
	Residence VARCHAR(15),
	Street VARCHAR(30),
	Mobile VARCHAR(12),
	Email VARCHAR(40),
);

CREATE TABLE store.Town ( -- table the naming format schemaname.tablename
	Town_Id INTEGER PRIMARY KEY, --
	Town_Name VARCHAR(30),
);

/* To modify an exisiting database structure the ALTER keyboard is used. */
ALTER TABLE store.Customer -- name of table for which structure will be modified
ALTER COLUMN Residence VARCHAR(25); -- The length of this attribute has been changed from 15 to 25

ALTER TABLE store.Customer
ADD Town_ID INTEGER;

/*	The above statement modifies the table store.Customer by adding a new attribute Town_ID of datatype INTEGER 
	below, modify customer last name up to 40 char*/

ALTER TABLE store.Customer
ALTER COLUMN Last_Name VARCHAR(40) NOT NULL;

/* To delete a database structure, we use DROP */

DROP TABLE store.Town;

/* The above statement will delete the table store.Town and all its data */

DROP TABLE store.Customer;

/* In order to delete the database VideoShop, we need to switch the use of the script back to master. */

USE master;
GO

DROP DATABASE VideoShop;

-- DML statements

/* To inset a new record into a table, the INSERT keyword is used */

INSERT INTO store.Customer  -- table where a new record/s will be added
VALUES (1,'Adam','Borg','20','Triq il-Giebja','99123456','ab@gmail.com',1);

/*	The above statements adds a new record to the table store.Customer. Each value must be inserted in the same
	order the attributes(Columns) are listed in the Object Explorer. A value must be inserted for each column. 
	Values for integers are not enclosed in single quotes but values for VARCHAR are enclosed in single quotes. */

INSERT INTO store.Customer (Customer_Id, First_Name, Last_Name, Mobile, Email) -- 1st clause
VALUES (2,'Dean','Grech','77343434','dg@gmail.com')

/*	In the above statement, a new record has been added but only values for the columns listed in the first clause
	and the values must be enterd in the order that the colums are listed. The columns omitted in the list must be
	columns that accept NULL values.*/

INSERT INTO store.Customer (Customer_Id, First_Name, Last_Name, Mobile, Email)	-- 2st clause
VALUES	(3,'Meg','Vella','77890000','mv@gmail.com'), -- add a comma when inserting mutiple records in the same statement
		(4,'Denise','Attard','99887766','da@gmail.com');

INSERT INTO store.Town (Town_Id, Town_Name)
VALUES	(1,'Paola'), 
		(2,'Mellieha'), 
		(3,'Tarxien'), 
		(4,'Mosta');

/* To read the data in a database, we use the keyword SELECT*/

SELECT * --* means all columns
FROM store.Customer;

/* The above statement returns all the columns for all the records in the table store.Customer */

SELECT First_Name, Last_Name, Mobile
FROM store.Customer
WHERE Mobile LIKE '77%'; -- The WHERE clause filters the records according to a condition.

/*	The condition in the above statement is to retuurn Mobie phones starting with the numbers 77.
	The % is a wildcard which represents 0 or more characters.
	to return 1 column of town ID - 2*/

SELECT Town_Name
FROM store.Town
WHERE Town_Id LIKE '2';

/*	To modify data in the database, we use the UPDATE keyword. */

UPDATE store.Customer -- the table is to be updated is store.Customer
SET Street = 'Triq il-Kbira' -- insert value 'Triq il-Kbira in the column Street
WHERE Customer_ID IN (3,4)-- this update applies to records with customer_Id  3 and 4

UPDATE store.Customer -- the table is to be updated is store.Customer
SET Town_Id = '3' -- insert value 'Triq il-Kbira in the column Street
WHERE Customer_ID IN (2,3,4)

/* The DELETE statement removes records from the database. */

DELETE FROM store.town -- the table from which data is to be deleted
WHERE Town_Id = 2; -- the WHERE clause filters the rows to be deleted

SELECT *
FROM store.Town;

DELETE FROM store.Town;

/* The above statement will delete all the records in the  table store. Town since the WHERE clause has been omitted.*/



/* TCL - Transaction Control is a set of statements that group a number of DML statements into a single unit of work.*/

BEGIN TRANSACTION; -- Denotes the begginning of a new transaction

INSERT INTO store.Customer (Customer_Id, First_Name, Last_Name, Mobile, Email)
VALUES (5,'Ella','Grech','77343435','eg@gmail.com')

SELECT *
FROM store.Customer;

COMMIT; -- Denote the end of the transaction

BEGIN TRANSACTION; -- Denotes the beggining of a second transaction

DELETE FROM store.Customer
WHERE Customer_Id = 5; -- This statement deletes Ella Grech's Record

SELECT *
FROM store.Customer; -- This statement should return 4 rows

ROLLBACK TRANSACTION; -- This undoes the DELETE statement

SELECT *
FROM store.Customer; -- this statement should return 5 rows

