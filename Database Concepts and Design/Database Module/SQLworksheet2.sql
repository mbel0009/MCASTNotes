CREATE DATABASE grocery;
GO

USE grocery;
GO

CREATE TABLE Products (
	ProductId INTEGER,
	Product VARCHAR(40),
	Price NUMERIC(4,2)
);

SELECT *
FROM Products;

ALTER TABLE Products
ADD Weight NUMERIC(5,2);

INSERT INTO Products(ProductId, Product, Price, Weight) VALUES
(1, 'Crisps',0.75,75),
(2, 'Cola',1.24,100),
(3, 'Peanuts',1.10,50);

UPDATE Products SET Price = 1.26
WHERE ProductId = 2

DELETE Products WHERE ProductId = 3;

BEGIN TRANSACTION Tr1;

DELETE Products;

SELECT * FROM Products;

ROLLBACK TRANSACTION Tr1;

SELECT * FROM Products;

DROP TABLE Products;