CREATE DATABASE SQLworksheet3;
GO

USE SQLworksheet3;
GO

CREATE TABLE Students (
	StudentID INTEGER,
	FirstName VARCHAR(25),
	LastName VARCHAR(25)
);

SELECT *
FROM Students;

ALTER TABLE Students
ADD Mobile Varchar(8);

INSERT INTO Students
VALUES	(1, 'Alex','Borg','79798888'),
		(2, 'Sarah', 'Gauci', '99878711'),
		(3, 'Paula', 'Zarb', '79881111 ');
		
UPDATE Students
SET Mobile = '99798888'
WHERE StudentID = 1;

BEGIN TRANSACTION TranA;
GO 

DELETE Students
WHERE StudentID = 3;

COMMIT TRANSACTION TranA;

DROP TABLE Students;